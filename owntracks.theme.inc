<?php

/**
 * @file
 * Preprocessors and theme functions of OwnTracks module.
 */

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Template\Attribute;

/**
 * Prepares variables for owntracks_map templates.
 *
 * Default template: owntracks-map.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - track: An array of Lat/Lang coordinates.
 */
function template_preprocess_owntracks_map(array &$variables) {
  $id = Html::getUniqueId('owntracks-map');
  $variables['attributes'] = new Attribute([
    'id' => $id,
    'class' => ['owntracks-map'],
  ]);

  $variables['#attached']['library'][] = 'owntracks/owntracks';
  $variables['#attached']['drupalSettings']['owntracks']['track'][$id] = $variables['track'];

  $config = \Drupal::config('owntracks.map.settings');
  $tileLayerUrl = (string) $config->get('tileLayerUrl');
  $tileLayerAttribution = (string) $config->get('tileLayerAttribution');
  $polylineColor = (string) $config->get('polylineColor');
  $variables['#attached']['drupalSettings']['owntracks']['map'] = [
    'tileLayerUrl' => Html::escape($tileLayerUrl),
    'tileLayerAttribution' => Xss::filterAdmin($tileLayerAttribution),
    'polylineColor' => Html::escape($polylineColor),
  ];
}
