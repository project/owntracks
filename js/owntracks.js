(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.owntracks = {
    attach(context) {
      $(once('owntracks-map', '.owntracks-map', context)).each(function () {
        /* global L */
        const id = $(this).attr('id');
        const map = L.map(id);
        const track = drupalSettings.owntracks.track[id];

        L.tileLayer(drupalSettings.owntracks.map.tileLayerUrl, {
          attribution: drupalSettings.owntracks.map.tileLayerAttribution,
        }).addTo(map);

        if (track !== null) {
          const polyline = L.polyline(track, {
            color: drupalSettings.owntracks.map.polylineColor,
          }).addTo(map);
          map.fitBounds(polyline.getBounds());

          $.each(track, function (i, e) {
            L.marker(e).addTo(map);
          });
        } else {
          map.setView([51.4833333, 7.2166667], 2);
        }
      });
    },
  };
})(jQuery, Drupal, drupalSettings);
